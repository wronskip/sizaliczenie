import org.deeplearning4j.eval.RegressionEvaluation;
import org.deeplearning4j.nn.api.OptimizationAlgorithm;
import org.deeplearning4j.nn.conf.MultiLayerConfiguration;
import org.deeplearning4j.nn.conf.NeuralNetConfiguration;
import org.deeplearning4j.nn.conf.layers.GravesLSTM;
import org.deeplearning4j.nn.conf.layers.RnnOutputLayer;
import org.deeplearning4j.nn.multilayer.MultiLayerNetwork;
import org.deeplearning4j.nn.weights.WeightInit;
import org.deeplearning4j.optimize.listeners.ScoreIterationListener;
import org.nd4j.linalg.activations.Activation;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.preprocessor.NormalizerMinMaxScaler;
import org.nd4j.linalg.learning.config.Nesterovs;
import org.nd4j.linalg.lossfunctions.LossFunctions;

import java.io.IOException;


public class Main {

    private static final NormalizerMinMaxScaler normalizer = new NormalizerMinMaxScaler(0, 1);

    public static void main(String... args) throws IOException, InterruptedException {
        DataProvider dataProvider = new FileDataProvider();

        DataSet trainData = dataProvider.getTrainData();
        DataSet testData = dataProvider.getTestData();

        normalize(trainData, testData);
        MultiLayerNetwork net = getNetwork();

        train(net, trainData, testData);

        INDArray predicted = predict(net, trainData, testData);

        denormalize(trainData, testData, predicted);

        new Chart(trainData, testData, predicted);
    }

    private static void normalize(DataSet trainData, DataSet testData) {
        normalizer.fitLabel(true);
        normalizer.fit(trainData);

        normalizer.transform(trainData);
        normalizer.transform(testData);
    }

    private static void denormalize(DataSet trainData, DataSet testData, INDArray predicted) {
        normalizer.revert(trainData);
        normalizer.revert(testData);
        normalizer.revertLabels(predicted);
    }

    private static MultiLayerNetwork getNetwork() {
        MultiLayerConfiguration conf = new NeuralNetConfiguration.Builder()
                .seed(140)
                .optimizationAlgo(OptimizationAlgorithm.STOCHASTIC_GRADIENT_DESCENT)
                .weightInit(WeightInit.XAVIER)
                .updater(new Nesterovs(Config.LEARNING_RATE, Config.MOMENTUM))
                .list()
                .layer(0, new GravesLSTM.Builder().activation(Activation.TANH).nIn(1).nOut(10)
                        .build())
                .layer(1, new RnnOutputLayer.Builder(LossFunctions.LossFunction.MSE)
                        .activation(Activation.IDENTITY).nIn(10).nOut(1).build())
                .pretrain(false).backprop(true).build();
        MultiLayerNetwork net = new MultiLayerNetwork(conf);
        net.init();
        net.setListeners(new ScoreIterationListener(20));
        return net;
    }

    private static void train(MultiLayerNetwork net, DataSet trainData, DataSet testData) {
        for (int i = 0; i < Config.EPOCHS; i++) {
            net.fit(trainData);
            System.out.println("Epoch " + i + " complete. Time series evaluation:");

            RegressionEvaluation evaluation = new RegressionEvaluation(1);
            INDArray features = testData.getFeatureMatrix();

            INDArray labels = testData.getLabels();
            INDArray predicted = net.output(features, false);

            evaluation.evalTimeSeries(labels, predicted);

            System.out.println(evaluation.stats());
        }
    }

    private static INDArray predict(MultiLayerNetwork net, DataSet trainData, DataSet testData) {
        net.rnnTimeStep(trainData.getFeatureMatrix());
        return net.output(testData.getFeatureMatrix(), false);

    }
}
