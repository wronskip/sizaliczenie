import org.nd4j.linalg.dataset.DataSet;

public interface DataProvider {

    DataSet getTrainData();

    DataSet getTestData();
}
