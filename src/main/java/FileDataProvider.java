import org.datavec.api.records.reader.SequenceRecordReader;
import org.datavec.api.records.reader.impl.csv.CSVSequenceRecordReader;
import org.datavec.api.split.NumberedFileInputSplit;
import org.deeplearning4j.datasets.datavec.SequenceRecordReaderDataSetIterator;
import org.nd4j.linalg.dataset.DataSet;
import org.nd4j.linalg.dataset.api.iterator.DataSetIterator;

import java.io.File;

public class FileDataProvider implements DataProvider {

    @Override
    public DataSet getTrainData() {
        String path = new File("src/main/resources").getAbsolutePath() + "/data/data-train-%d.csv";
        return getIteratorFor(path).next();
    }

    @Override
    public DataSet getTestData() {
        String path = new File("src/main/resources").getAbsolutePath() + "/data/data-test-%d.csv";
        return getIteratorFor(path).next();
    }

    private DataSetIterator getIteratorFor(String path) {
        SequenceRecordReader trainReader = new CSVSequenceRecordReader(0, ";");
        handleException(() -> trainReader.initialize(new NumberedFileInputSplit(path, 0, 0)));
        return new SequenceRecordReaderDataSetIterator(trainReader, Config.MINI_BATCH_SIZE, -1, 1, true);
    }

    private void handleException(CheckedRunnable runnable) {
        try {
            runnable.run();
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(-1);
        }
    }
}
