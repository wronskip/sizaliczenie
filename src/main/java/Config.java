
class Config {

    private Config() {
    }

    static final int EPOCHS = 35;
    static final double LEARNING_RATE = 0.00015d;
    static final int MINI_BATCH_SIZE = 32;
    static final double MOMENTUM = 0.9d;
}
